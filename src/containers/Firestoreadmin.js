import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router'
import firebase from '../config/database'
import Fields from '../components/fields/Fields.js'
import Radio from '../components/fields/Radio.js'
import File from '../components/fields/File.js'
import Input from '../components/fields/Input.js';
import Table from '../components/tables/Table.js'
import Config from '../config/app';
import Common from '../common.js';
import Notification from '../components/Notification';
import SkyLight from 'react-skylight';
import INSERT_STRUCTURE from "../config/firestoreschema.js"
import FirebasePaginator from "firebase-paginator"
import NavBar from '../components/NavBar'
import moment from 'moment';
import Image from '../components/fields/Image.js';
import * as firebaseREF from 'firebase';
//import trim from 'trim';
require("firebase/firestore");

const ROUTER_PATH = "/firestoreadmin/";
var Loader = require('halogen/PulseLoader');

class Firestoreadmin extends Component {

  constructor(props) {
    super(props);


    //Create initial step
    this.state = {
      fields: {}, //The editable fields, textboxes, checkbox, img upload etc..
      arrays: {}, //The array of data
      elements: [], //The elements - objects to present
      elementsInArray: [], //The elements put in array
      directValue: "", //Direct access to the value of the current path, when the value is string
      firebasePath: "",
      arrayNames: [],
      currentMenu: {},
      completePath: "",
      lastSub: "",
      isJustArray: false,
      currentInsertStructure: null,
      notifications: [],
      lastPathItem: "",
      pathToDelete: null,
      isItArrayItemToDelete: false,
      page: 1,
      documents: [],
      collections: [],
      currentCollectionName: "",
      isCollection: false,
      isDocument: false,
      keyToDelete: null,
      theSubLink: null,
      fieldsOfOnsert: null,
      isLoading: false,
      showAddCollection: "test",
      userinfo: [],
      full_name: "",
      father_name: "",
      date_of_birth: "",
      mother_name: "",
      gender: "",
      address: "",
      mobile_number: "",
      email: "",
      aadhar_number: "",
      photo_url: "",
      sports: "",
      category: "",
      coach_id: "",
      other_doc: "",
      errors: {},

    };

    //Bind function to this

    this.getCollectionDataFromFireStore = this.getCollectionDataFromFireStore.bind(this);
    this.resetDataFunction = this.resetDataFunction.bind(this);
    this.processRecords = this.processRecords.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.cancelDelete = this.cancelDelete.bind(this);
    this.cancelAddFirstItem = this.cancelAddFirstItem.bind(this);
    this.doDelete = this.doDelete.bind(this);
    this.deleteFieldAction = this.deleteFieldAction.bind(this);
    this.refreshDataAndHideNotification = this.refreshDataAndHideNotification.bind(this);
    this.addKey = this.addKey.bind(this);
    this.showSubItems = this.showSubItems.bind(this);
    this.updatePartOfObject = this.updatePartOfObject.bind(this);
    this.addDocumentToCollection = this.addDocumentToCollection.bind(this);
    this.addItemToArray = this.addItemToArray.bind(this);
    this.addnewathelete = this.addnewathelete.bind(this);
    this.formValueCapture = this.formValueCapture.bind(this);
    this.ionViewDidLoad = this.ionViewDidLoad.bind(this);
  }

  ionViewDidLoad() {
    this.userId = firebase.auth().currentUser.uid
    alert(this.userId);
  }

  //ionViewDidLoad();
  /**
   * Step 0a
   * Start getting data
   */
  componentDidMount() {
    //this.findFirestorePath();
    this.getMeTheFirestorePath();
    window.sidebarInit();

  }

  /**
  * Step 0b
  * Resets data function
  */
  resetDataFunction() {
    var newState = {};
    newState.documents = [];
    newState.collections = [];
    newState.currentCollectionName = "";
    newState.fieldsAsArray = [];
    newState.arrayNames = [];
    newState.fields = [];
    newState.arrays = [];
    newState.elements = [];
    newState.elementsInArray = [];
    newState.theSubLink = null;

    this.setState(newState);
    //this.findFirestorePath();
    this.getMeTheFirestorePath();
  }

  /**
   * Step 0c
   * componentWillReceiveProps event of React, fires when component is mounted and ready to display
   * Start connection to firebase
   */
  componentWillReceiveProps(nextProps, nextState) {
    console.log("Next SUB: " + nextProps.params.sub);
    console.log("Prev SUB : " + this.props.params.sub);
    if (nextProps.params.sub == this.props.params.sub) {
      console.log("update now");
      this.setState({ isLoading: true })
      this.resetDataFunction();
    }
  }

  /**
   * Step 0d
   * getMeTheFirestorePath created firestore path based on the router parh
   */
  getMeTheFirestorePath() {
    this.userId = firebase.app.auth().currentUser.uid;
    var db = firebase.app.firestore();
    var docRef = db.collection("clubmaps").doc("user" + this.userId);
    docRef.get().then(doc => {
      if (doc.exists) {
        var pathdoc = doc.data();
        this.findFirestorePath(pathdoc.dbpath);
      }
      else { alert("error"); }
    })

    // var thePath=(this.props.route.path.replace(ROUTER_PATH,"").replace(":sub",""))+(this.props.params&&this.props.params.sub?this.props.params.sub:"").replace(/\+/g,"/");;
    // return thePath;
  }

  /**
   * Step 1
   * Finds out the Firestore path
   * Also creates the path that will be used to access the insert
   */
  findFirestorePath(firebasePath) {
    var pathData = {}
    if (this.props.params && this.props.params.sub) {
      pathData.lastSub = this.props.params.sub;
    }

    //Find the firestore path
    //var firebasePath= this.getMeTheFirestorePath();
    pathData.firebasePath = firebasePath + "/" + pathData.lastSub;
    pathData.firebasePath = pathData.firebasePath.replace(/\+/g, "/");

    pathData.lastSub = pathData.firebasePath.replace(/\//g, "+");

    //Find last path - the last item
    var subPath = pathData.lastSub;//this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    pathData.lastPathItem = Common.capitalizeFirstLetter(items[items.length - 1]);
    pathData.completePath = subPath;

    //Save this in state
    this.setState(pathData);

    //Go to next step of finding the collection data
    this.getCollectionDataFromFireStore(pathData.firebasePath);
  }

  /**
  * Step 2
  * Connect to firestore to get the current item we need
  * @param {String} collection - this infact can be collection or document
  */
  getCollectionDataFromFireStore(collection) {

    //Create the segmments based on the path / collection we have
    var segments = collection.split("/");
    var lastSegment = segments[segments.length - 1];

    //Is this a call to a collections data
    var isCollection = segments.length % 2;

    //Reference to this
    var _this = this;

    //Save know info for now
    this.setState({
      currentCollectionName: segments[segments.length - 1],
      isCollection: isCollection,
      isDocument: !isCollection,
    })

    //Get reference to firestore
    var db = firebase.app.firestore();

    //Here, we will save the documents from collection
    var documents = [];

    if (isCollection) {
      //COLLECTIONS - GET DOCUMENTS 

      db.collection(collection).get().then(function (querySnapshot) {
        var datacCount = 0;
        querySnapshot.forEach(function (doc) {

          //Increment counter
          datacCount++;

          //Get the object
          var currentDocument = doc.data();

          //Sace uidOfFirebase inside him
          currentDocument.uidOfFirebase = doc.id;

          console.log(doc.id, " => ", currentDocument);

          //Save in the list of documents
          documents.push(currentDocument)
        });
        console.log("DOCS----");
        console.log(documents);

        //Save the douments in the sate
        _this.setState({
          isLoading: false,
          documents: documents,
          showAddCollection: datacCount == 0 ? collection : ""
        })
        if (datacCount == 0) {
          _this.refs.addCollectionDialog.show();
        }
        console.log(_this.state.documents);
      });
    } else {
      //DOCUMENT - GET FIELDS && COLLECTIONS
      var referenceToCollection = collection.replace("/" + lastSegment, "");

      //Create reference to the document itseld
      var docRef = db.collection(referenceToCollection).doc(lastSegment);

      //Get the starting collectoin
      var parrentCollection = segments;
      parrentCollection.splice(-1, 1);

      //Find the collections of this document
      this.findDocumentCollections(parrentCollection);

      docRef.get().then(function (doc) {
        if (doc.exists) {
          console.log("Document data:", doc.data());

          //Directly process the data
          _this.processRecords(doc.data())
        } else {
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });
    }
  }

  /**
   * Step 3
   * findDocumentCollections - what collections should we display / currently there is no way to get collection form docuemnt
   * @param {Array} chunks - the collection / documents
   */
  findDocumentCollections(chunks) {

    console.log("Search for the schema now of " + chunks);

    //At start is the complete schema
    var theInsertSchemaObject = INSERT_STRUCTURE;
    var cuurrentFields = null;
    console.log("CHUNKS");
    console.log(chunks);

    //Foreach chunks, find the collections / fields
    chunks.map((item, index) => {
      console.log("current chunk:" + item);

      //Also make the last object any
      //In the process, check if we have each element in our schema
      if (theInsertSchemaObject != null && theInsertSchemaObject && theInsertSchemaObject[item] && theInsertSchemaObject[item]['collections']) {
        var isLastObject = (index == (chunks.length - 1));

        if (isLastObject && theInsertSchemaObject != null && theInsertSchemaObject[item] && theInsertSchemaObject[item]['fields']) {
          cuurrentFields = theInsertSchemaObject[item]['fields'];
        }

        if (isLastObject && theInsertSchemaObject != null) {
          //It is last
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        } else {
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        }
      } else {
        theInsertSchemaObject = [];
      }
      console.log("Current schema");
      console.log(theInsertSchemaObject);


    })

    //Save the collection to be shown as button and fieldsOfOnsert that will be used on inserting object
    this.setState({ collections: theInsertSchemaObject, fieldsOfOnsert: cuurrentFields })
  }

  /**
   * Step 4
   * Processes received records from firebase
   * @param {Object} records
   */
  processRecords(records) {
    console.log(records);

    var fields = {};
    var arrays = {};
    var elements = [];
    var elementsInArray = [];
    var newState = {};
    var directValue = "";
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.elements = elements;
    newState.directValue = directValue;
    newState.elementsInArray = elementsInArray;
    newState.records = null;

    this.setState(newState);

    //Each display is consisted of
    //Fields   - This are string, numbers, photos, dates etc...
    //Arrays   - Arrays of data, ex items:[0:{},1:{},2:{}...]
    //         - Or object with prefixes that match in array
    //Elements - Object that don't match in any prefix for Join - They are represented as buttons.

    //In FireStore
    //GeoPoint
    //DocumentReference

    //If record is of type array , then there is no need for parsing, just directly add the record in the arrays list

    console.log(Common.getClass(records));
    if (Common.getClass(records) == "Array") {
      //Get the last name
      console.log("This is array");
      var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
      var allPathItems = subPath.split("+");
      console.log(allPathItems)
      if (allPathItems.length > 0) {
        var lastItem = allPathItems[allPathItems.length - 1];
        console.log(lastItem);
        arrays[lastItem] = records;

      }
      //this.setState({"arrays":this.state.arrays.push(records)})
    } else if (Common.getClass(records) == "Object") {
      //Parse the Object record
      for (var key in records) {
        if (records.hasOwnProperty(key)) {
          var currentElementClasss = Common.getClass(records[key]);
          console.log(key + "'s class is: " + currentElementClasss);

          //Add the items by their type
          if (currentElementClasss == "Array") {
            //Add it in the arrays  list
            arrays[key] = records[key];
          } else if (currentElementClasss == "Object") {
            //Add it in the elements list
            var isElementMentForTheArray = false; //Do we have to put this object in the array
            for (var i = 0; i < Config.adminConfig.prefixForJoin.length; i++) {
              if (key.indexOf(Config.adminConfig.prefixForJoin[i]) > -1) {
                isElementMentForTheArray = true;
                break;
              }
            }

            var objToInsert = records[key];
            //alert(key);
            objToInsert.uidOfFirebase = key;

            if (isElementMentForTheArray) {
              //Add this to the merged elements
              elementsInArray.push(objToInsert);
            } else {
              //Add just to elements
              elements.push(objToInsert);
            }

          } else if (currentElementClasss != "undefined" && currentElementClasss != "null") {
            //This is string, number, or Boolean
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "GeoPoint") {
            //This is GeoPOint
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "DocumentReference") {
            //This is DocumentReference
            //Add it to the fields list
            fields[key] = records[key];
          }

        }
      }
    } if (Common.getClass(records) == "String") {
      console.log("We have direct value of string");
      directValue = records;
    }

    //Convert fields from object to array
    var fieldsAsArray = [];
    console.log("Add the items now inside fieldsAsArray");
    console.log("Current schema");
    console.log(this.state.currentInsertStructure)
    //currentInsertStructure
    var keysFromFirebase = Object.keys(fields);
    console.log("keysFromFirebase")
    console.log(keysFromFirebase)
    var keysFromSchema = Object.keys(this.state.currentInsertStructure || {});
    console.log("keysFromSchema")
    console.log(keysFromSchema)

    keysFromSchema.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        var indexOfElementInFirebaseObject = keysFromFirebase.indexOf(key);
        if (indexOfElementInFirebaseObject > -1) {
          keysFromFirebase.splice(indexOfElementInFirebaseObject, 1);
        }
      }
    });

    console.log("keysFromFirebase")
    console.log(keysFromFirebase)

    keysFromFirebase.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        fieldsAsArray.push({ "theKey": key, "value": fields[key] })
      }
    });



    //Get all array names
    var arrayNames = [];
    Object.keys(arrays).forEach((key) => {
      arrayNames.push(key)
    });

    var newState = {};
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.isJustArray = Common.getClass(records) == "Array";
    newState.elements = elements;
    newState.elementsInArray = elementsInArray;
    newState.directValue = directValue;
    newState.records = records;
    newState.isLoading = false;

    console.log("THE elements")
    console.log(elements);

    //Set the new state
    this.setState(newState);

    //Additional init, set the DataTime, check format if something goes wrong
    window.additionalInit();
  }

  /**
   *
   * Create R Update D
   *
   */

  /**
  * processValueToSave  - helper for saving in Firestore , converts value to correct format
  * @param {value} value
  * @param {type} type of field
  */
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }

  /**
  * updatePartOfObject  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {String} firebasePath current firestore path
  * @param {String} byGivvenSubLink force link to field
  * @param {Function} callback function after action
  */
  updatePartOfObject(key, value, dorefresh = false, type = null, firebasePath, byGivvenSubLink = null, callback = null) {
    var subLink = this.state.theSubLink;
    if (byGivvenSubLink != null) {
      subLink = byGivvenSubLink;
    }
    console.log("Sub save " + key + " to " + value + " and the path is " + firebasePath + " and theSubLink is " + subLink);
    var chunks = subLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    var _this = this;
    //First get the document
    //DOCUMENT - GET FIELDS && COLLECTIONS
    var docRef = firebase.app.firestore().doc(firebasePath);
    docRef.get().then(function (doc) {
      if (doc.exists) {
        var numChunks = chunks.length - 1;
        var doc = doc.data();
        if (value == "DELETE_VALUE") {
          if (numChunks == 2) {
            doc[chunks[1]].splice(chunks[2], 1);
          }
          if (numChunks == 1) {
            doc[chunks[1]] = null;
          }
        } else {
          //Normal update, or insert
          if (numChunks == 2) {
            doc[chunks[1]][chunks[2]] = value
          }
          if (numChunks == 1) {
            doc[chunks[1]][key] = value
          }
        }

        console.log("Document data:", doc);
        _this.updateAction(chunks[1], doc[chunks[1]], dorefresh, null, true)
        if (callback) {
          callback();
        }

        //alert(chunks.length-1);
        //_this.processRecords(doc.data())
        //console.log(doc);

      } else {
        console.log("No such document!");
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });

  }



  /**
  * Firebase update based on key / value,
  * This function also sets derect name and value
  * @param {String} key
  * @param {String} value
  */

  /**
  * updateAction  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {Boolean} forceObjectSave force saving sub object
  */
  updateAction(key, value, dorefresh = true, type = null, forceObjectSave = false) {
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/firestoreadmin/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {
        var db = firebase.app.firestore();

        var databaseRef = db.doc(firebasePath);
        var updateObj = {};
        updateObj[key] = value;
        databaseRef.set(updateObj, { merge: true });
      }

    }
  }

  /**
  * addDocumentToCollection  - used recursivly to add collection's document's collections
  * @param {String} name name of the collection
  * @param {FirestoreReference} reference
  */
  addDocumentToCollection(name, reference = null) {

    var pathChunks = this.state.firebasePath.split("/");
    pathChunks.pop();
    var withoutLast = pathChunks.join("/");
    console.log(name + " vs " + withoutLast);
    //Find the fields to be inserted
    var theInsertSchemaObject = INSERT_STRUCTURE[name].fields;
    console.log(JSON.stringify(theInsertSchemaObject));

    //Find the collections to be inserted
    var theInsertSchemaCollections = INSERT_STRUCTURE[name].collections;
    console.log(JSON.stringify(theInsertSchemaCollections));

    //Reference to root firestore or existing document reference
    var db = reference == null ? (pathChunks.length > 1 ? firebase.app.firestore().doc(withoutLast) : firebase.app.firestore()) : reference;

    //Check type of insert
    var isTimestamp = Config.adminConfig.methodOfInsertingNewObjects == "timestamp"

    //Create new element
    var newElementRef = isTimestamp ? db.collection(name).doc(Date.now()) : db.collection(name).doc()

    //Add data to the new element
    //newElementRef.set(theInsertSchemaObject)

    //Go over sub collection and insert them
    for (var i = 0; i < theInsertSchemaCollections.length; i++) {
      this.addDocumentToCollection(theInsertSchemaCollections[i], newElementRef)
    }


    //Show the notification on root element
    if (reference == null) {
      this.cancelAddFirstItem();
      this.setState({ notifications: [{ type: "success", content: "Element added. You can find it in the table bellow." }] });
      this.refreshDataAndHideNotification();
    }
  }

  /**
  * addKey
  * Adds key in our list of fields in firestore
  */
  addKey() {
    if (this.state.NAME_OF_THE_NEW_KEY && this.state.NAME_OF_THE_NEW_KEY.length > 0) {

      if (this.state.VALUE_OF_THE_NEW_KEY && this.state.VALUE_OF_THE_NEW_KEY.length > 0) {

        this.setState({ notifications: [{ type: "success", content: "New key added." }] });
        this.updateAction(this.state.NAME_OF_THE_NEW_KEY, this.state.VALUE_OF_THE_NEW_KEY);
        this.refs.simpleDialog.hide();
        this.refreshDataAndHideNotification();
      }
    }
  }

  /**
  * addItemToArray  - add item to array
  * @param {String} name name of the array
  * @param {Number} howLongItIs count of items, to know the next index
  */
  addItemToArray(name, howLongItIs) {
    console.log("Add item to array " + name);
    console.log("Is just array " + this.state.isJustArray);

    console.log("Data ");
    console.log(this.state.fieldsOfOnsert);

    var dataToInsert = null;
    var correctPathToInsertIn = "";
    if (this.state.fieldsOfOnsert) {
      if (this.state.isJustArray) {
        console.log("THIS IS Array")
        dataToInsert = this.state.fieldsOfOnsert[0];
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      } else {
        dataToInsert = this.state.fieldsOfOnsert[name];
        dataToInsert = dataToInsert ? dataToInsert[0] : null;
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + name + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      }
    }

    console.log("Data to insert");
    console.log(dataToInsert);
    console.log("Path to insert");
    console.log(correctPathToInsertIn);

    var _this = this;
    this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", dataToInsert, true, null, this.state.firebasePath, correctPathToInsertIn, function (e) {
      _this.setState({ notifications: [{ type: "success", content: "New element added." }] });
      _this.refreshDataAndHideNotification();
    })
  }

  /**
  *
  * C Read U D
  *
  */

  /**
  * showSubItems - displays sub object, mimics opening of new page
  * @param {String} theSubLink , direct link to the sub object
  */
  showSubItems(theSubLink) {
    var chunks = theSubLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    this.setState({
      itemOfInterest: chunks[1],
      theSubLink: theSubLink,
    })
    var items = this.state.records;
    for (var i = 1; i < chunks.length; i++) {
      console.log(chunks[i]);
      items = items[chunks[i]];
    }
    console.log("--- NEW ITEMS ");
    console.log(items)
    this.processRecords(items);
  }

  /**
  *
  * C R U Delete
  *
  */

  /**
  * deleteFieldAction - displays sub object, mimics opening of new page
  * @param {String} key to be updated
  * @param {Boolean} isItArrayItem 
  * @param {String} theLink 
  */
  deleteFieldAction(key, isItArrayItem = false, theLink = null) {
    console.log("Delete " + key);
    console.log(theLink);
    if (theLink != null) {
      theLink = theLink.replace("/firestoreadmin", "");
    }
    if (isNaN(key)) {
      isItArrayItem = false;
    }
    console.log("Is it array: " + isItArrayItem);
    var firebasePathToDelete = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (key != null) {
      //firebasePathToDelete+=("/"+key)
    }

    console.log("firebasePath for delete:" + firebasePathToDelete);
    this.setState({ pathToDelete: theLink ? theLink : firebasePathToDelete, isItArrayItemToDelete: isItArrayItem, keyToDelete: theLink ? "" : key });
    window.scrollTo(0, 0);
    this.refs.deleteDialog.show();

  }

  /**
  * doDelete - do the actual deleting based on the data in the state
  */
  doDelete() {
    var _this = this;

    var completeDeletePath = this.state.pathToDelete + "/" + this.state.keyToDelete;
    var collName = this.state.firebasePath + "/" + this.state.completePath;

    if (this.state.pathToDelete.indexOf(Config.adminConfig.urlSeparatorFirestoreSubArray) > -1) {
      //Sub data
      _this.refs.deleteDialog.hide();
      this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {
        _this.setState({ notifications: [{ type: "success", content: "Element deleted." }] });
        _this.refreshDataAndHideNotification();
      })
    } else {
      //Normal data

      var chunks = completeDeletePath.split("/");

      var db = firebase.app.firestore();


      if (chunks.length % 2) {
        //odd
        //Delete fields from docuemnt
        var refToDoc = db.doc(this.state.pathToDelete);

        // Remove the 'capital' field from the document
        var deleteAction = {};
        deleteAction[this.state.keyToDelete] = firebaseREF.firestore.FieldValue.delete();
        var removeKey = refToDoc.update(deleteAction).then(function () {
          console.log("Document successfully deleted!");
          _this.refs.deleteDialog.hide();
          _this.setState({ keyToDelete: null, pathToDelete: null, notifications: [{ type: "success", content: "Field is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });
      } else {
        //even
        //delete document from collection
        //alert("Delete document "+completeDeletePath);
        //db.collection(this.state.pathToDelete).doc(this.state.keyToDelete).delete().then(function() {
        db.collection(collName).doc(this.state.keyToDelete).delete().then(function () {
          console.log("Document successfully deleted!");
          _this.refs.deleteDialog.hide();
          _this.setState({ pathToDelete: null, notifications: [{ type: "success", content: "Field is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });

      }

    }




    /*firebase.database().ref(this.state.pathToDelete).set(null).then((e)=>{
      console.log("Delete res: "+e)
      this.refs.deleteDialog.hide();
      this.setState({keyToDelete:null,pathToDelete:null,notifications:[{type:"success",content:"Field is deleted."}]});
      this.refreshDataAndHideNotification();

    })*/
  }

  /**
  * cancelDelete - user click on cancel
  */
  cancelDelete() {
    console.log("Cancel Delete");
    this.refs.deleteDialog.hide()
  }

  cancelAddFirstItem() {
    console.log("Cancel Add");
    this.refs.addCollectionDialog.hide()
  }



  /**
  *
  * UI GENERATORS
  *
  */

  /**
  * This function finds the headers for the current menu
  * @param firebasePath - we will use current firebasePath to find the current menu
  */
  findHeadersBasedOnPath(firebasePath) {
    var headers = null;

    var itemFound = false;
    var navigation = Config.navigation;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
      if (navigation[i].path == firebasePath && navigation[i].tableFields && navigation[i].link == "firestoreadmin") {
        headers = navigation[i].tableFields;
        itemFound = true;
      }

      //Look into the sub menus
      if (navigation[i].subMenus) {
        for (var j = 0; j < navigation[i].subMenus.length; j++) {
          if (navigation[i].subMenus[j].path == firebasePath && navigation[i].subMenus[j].tableFields && navigation[i].subMenus[j].link == "firestoreadmin") {
            headers = navigation[i].subMenus[j].tableFields;
            itemFound = true;
          }
        }
      }
    }
    return headers;
  }

  /**
  * makeCollectionTable
  * Creates single collection documents
  */
  makeCollectionTable() {
    var name = this.state.currentCollectionName;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          {/*begin pp_add*/}
          {/*
                  <a  onClick={()=>{this.addDocumentToCollection(name)}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}> */}
          <a style={{ cursor: "pointer" }} onClick={() => { this.refs.addnewitemDialog.show() }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
            {/*end pp_add*/}

            <i className="material-icons">add</i>
          </div></a>
          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={true}
                name={name}
                routerPath={this.props.route.path}
                isJustArray={false}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.documents}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Creates single array section
   * @param {String} name, used as key also
   */
  makeArrayCard(name) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <a onClick={() => { this.addItemToArray(name, this.state.arrays[name].length) }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
            <i className="material-icons">add</i>
          </div></a>
          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={false} name={name}
                routerPath={this.props.route.path}
                isJustArray={this.state.isJustArray}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.arrays[name]} />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Creates  table section for the elements object
   * @param {String} name, used as key also
   */
  makeTableCardForElementsInArray() {
    var name = this.state.lastPathItem;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)} deleteFieldAction={this.deleteFieldAction} fromObjectInArray={true} name={name} routerPath={this.props.route.path} isJustArray={this.state.isJustArray} sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""} data={this.state.elementsInArray}>
              </Table>
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
    * Creates direct value section
    * @param {String} value, valu of the current path
    */
  makeValueCard(value) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <div className="card-content">
            <h4 className="card-title">Value</h4>
            <div className="toolbar">
            </div>
            <div>
              <Input updateAction={this.updateAction} className="" theKey="DIRECT_VALUE_OF_CURRENT_PATH" value={value} />
            </div>
          </div>
        </div>
      </div>
    )
  }


  /**
   * generateBreadCrumb
   */
  generateBreadCrumb() {
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    var path = "/firestoreadmin/"
    return (<div>{items.map((item, index) => {
      if (index == 0) {
        path += item;
      } else {
        path += "+" + item;
      }

      return (<Link className="navbar-brand" to={path}>{item} <span className="breadcrumbSeparator">{index == items.length - 1 ? "" : "/"}</span><div className="ripple-container"></div></Link>)
    })}</div>)
  }

  /**
   * generateNotifications
   * @param {Object} item - notification to be created
   */
  generateNotifications(item) {
    return (
      <div className="col-md-12">
        <Notification type={item.type} >{item.content}</Notification>
      </div>
    )
  }

  /**
  * refreshDataAndHideNotification
  * @param {Boolean} refreshData 
  * @param {Number} time 
  */
  refreshDataAndHideNotification(refreshData = true, time = 3000) {
    //Refresh data,
    if (refreshData) {
      this.resetDataFunction();
    }

    //Hide notifications
    setTimeout(function () { this.setState({ notifications: [] }) }.bind(this), time);
  }


  formValueCapture(k, v) {
    //alert(k);
    this.setState({ [k]: v });
  }

  // handleValidation() {
  //   //let fields = this.state.fields;
  //   let errors = {};
  //   let formIsValid = true;

  //   //Name
  //   if (!this.state.Fullname) {
  //     formIsValid = false;
  //     errors["name"] = "Fullname Cannot be empty";
  //   }
  //   else {
  //     if (typeof this.state.Fullname !== "undefined") {
  //       if (!this.state.Fullname.match(/^[a-zA-Z]+$/)) {
  //         formIsValid = false;
  //         errors["name"] = "Fullname Only letters";
  //       }
  //     }
  //   }

  //   //FatherName
  //   if (!this.state.FatherName) {
  //     formIsValid = false;
  //     errors["fathername"] = "FatherName Cannot be empty";
  //   }
  //   else {
  //     if (typeof this.state.FatherName !== "undefined") {
  //       if (!this.state.FatherName.match(/^[a-zA-Z]+$/)) {
  //         formIsValid = false;
  //         errors["fathername"] = "FatherName Only letters";
  //       }
  //     }
  //   }

  //   //MotherName
  //   if (!this.state.MotherName) {
  //     formIsValid = false;
  //     errors["mothername"] = "MotherName Cannot be empty";
  //   }
  //   else {
  //     if (typeof this.state.MotherName !== "undefined") {
  //       if (!this.state.MotherName.match(/^[a-zA-Z]+$/)) {
  //         formIsValid = false;
  //         errors["mothername"] = "MotherName Only letters";
  //       }
  //     }
  //   }

  //   //DOB
  //   if (!this.state.DOB) {
  //     formIsValid = false;
  //     errors["dob"] = "DOB Cannot be empty";
  //   }
  //   else {
  //     if (typeof this.state.DOB !== "undefined") {
  //       if (!this.state.DOB.match(/^[0-9]+$/)) {
  //         formIsValid = false;
  //         errors["dob"] = "DOB Only Number";
  //       }
  //     }
  //   }

  //   //Gender
  //   if (!this.state.Gender) {
  //     formIsValid = false;
  //     errors["gender"] = "Gender Cannot be empty";
  //   }

  //   //Address
  //   if (!this.state.Address) {
  //     formIsValid = false;
  //     errors["address"] = "Address Cannot be empty";
  //   }

  //   //Email
  //   if (!this.state.Email) {
  //     formIsValid = false;
  //     errors["email"] = "Email Cannot be empty";
  //   }
  //   else {
  //     if (typeof this.state.Email !== "undefined") {
  //       let lastAtPos = this.state.Email.lastIndexOf('@');
  //       let lastDotPos = this.state.Email.lastIndexOf('.');

  //       if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.Email.indexOf('@@') == -1 && lastDotPos > 2 && (this.state.Email.length - lastDotPos) > 2)) {
  //         formIsValid = false;
  //         errors["email"] = "Email is not valid";
  //       }
  //     }
  //   }



  //   this.setState({ errors: errors });
  //   return formIsValid;
  // }
  addnewathelete(e) {
    e.preventDefault(); // <- prevent form submit from reloading the page

    /* Send the message to Firebase */

    var db = firebase.app.firestore();
    var collName = this.state.firebasePath + "/" + this.state.completePath;

    // this.inputData();
    if (true)//this.handleValidation()) 
    {
      db.collection(collName).add({
        full_name: this.state.full_name,
        father_name: this.state.father_name,
        date_of_birth: this.state.date_of_birth,
        mother_name: this.state.mother_name,
        gender: this.state.gender,
        address: this.state.address,
        mobile_number: this.state.mobile_number,
        email: this.state.email,
        aadhar_number: this.state.aadhar_number,
        photo_url: this.state.photo_url,
        sports: this.state.sports,
        category: this.state.category,
        coach_id: this.state.coach_id,
        other_doc: this.state.other_doc
        // Name: this.state.Fullname,
        // FatherName: this.state.FatherName,
        // MotherName: this.state.MotherName,
        // DOB: this.state.DOB,
        // Gender: this.state.Gender,
        // Address: this.state.Address,
        // MobileNumber: this.state.Mobileno,
        // Email: this.state.Email,
        // Aadharno: this.state.Aadharno,
        // Passport: this.state.Passport,
        // Otherdoc: this.state.Otherdoc
      })
      this.state.full_name = '',
        this.state.father_name = '',
        this.state.mother_name = '',
        this.state.date_of_birth = '',
        this.state.gender = '',
        this.state.address = '',
        this.state.mobile_number = '',
        this.state.email = '',
        this.state.aadhar_number = '',
        this.state.photo_url = '',
        this.state.sports = '',
        this.state.category = '',
        this.state.coach_id = '',
        this.state.other_doc = ''
      this.setState({ notifications: [{ type: "success", content: "Element added Successfully." }] })
      this.refs.addnewitemDialog.hide()
      this.refreshDataAndHideNotification()
        .then(function (docRef) {
          //alert("Document written with ID: "+ docRef.id);

        })
        .catch(function (error) {
          alert("Error adding document: " + error);
        });

    }
  }
  //MAIN RENDER FUNCTION 
  render() {
    //begin pp_add
    var addnewitemDialog = {
      width: '70%',
      height: 'auto',
      marginLeft: '-35%',
      position: 'absolute',
      padding: '0px'
    };
    //end pp_add
    return (
      <div className="content">
        <NavBar>{this.generateBreadCrumb()}</NavBar>


        <div className="content" sub={this.state.lastSub}>

          <div className="container-fluid">

            <div style={{ textAlign: 'center' }}>
              {/* LOADER */}
              {this.state.isLoading ? <Loader color="#8637AD" size="12px" margin="4px" /> : ""}
            </div>

            {/* NOTIFICATIONS */}
            {this.state.notifications ? this.state.notifications.map((notification) => {
              return this.generateNotifications(notification)
            }) : ""}

            {/* Documents in collection */}
            {this.state.isCollection && this.state.documents.length > 0 ? this.makeCollectionTable() : ""}

            {/* DIRECT VALUE */}
            {this.state.directValue && this.state.directValue.length > 0 ? this.makeValueCard(this.state.directValue) : ""}


            {/* FIELDS */}
            {this.state.fieldsAsArray && this.state.fieldsAsArray.length > 0 ? (<div className="col-md-12">
              <div className="card">
                {/*
                <a  onClick={()=>{this.refs.simpleDialog.show()}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}>
                    <i className="material-icons">add</i>
                </div></a>*/}
                <form className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{Common.capitalizeFirstLetter(Config.adminConfig.fieldBoxName)}</h4>
                  </div>
                  {this.state.fieldsAsArray ? this.state.fieldsAsArray.map((item) => {

                    return (
                      <Fields
                        isFirestore={true}
                        parentKey={null}
                        key={item.theKey + this.state.lastSub}
                        deleteFieldAction={this.deleteFieldAction}
                        updateAction={this.updateAction}
                        theKey={item.theKey}
                        value={item.value} />)


                  }) : ""}
                  <div
                    className="text-center">

                    <button
                      className="btn btn-rose btn-wd margin-bottom-10px"
                      value="Save">Save</button>

                    <Link to='/firestoreadmin/athletes'>
                      <button
                        className="btn btn-rose btn-wd margin-bottom-10px"
                        value="Cancel">Cancel</button>
                    </Link>

                  </div>

                </form>
              </div>
            </div>) : ""}


            {/* COLLECTIONS */}
            {this.state.theSubLink == null && this.state.isDocument && this.state.collections && this.state.collections.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{"Collections"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.theSubLink == null && this.state.collections ? this.state.collections.map((item) => {
                      var theLink = "/firestoreadmin/" + this.state.completePath + Config.adminConfig.urlSeparator + item;
                      return (<Link to={theLink}><a className="btn">{item}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}

            {/* ARRAYS */}
            {this.state.arrayNames ? this.state.arrayNames.map((key) => {
              return this.makeArrayCard(key)
            }) : ""}

            {/* ELEMENTS MERGED IN ARRAY */}
            {this.state.elementsInArray && this.state.elementsInArray.length > 0 ? (this.makeTableCardForElementsInArray()) : ""}

            {/* ELEMENTS */}
            {this.state.elements && this.state.elements.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{this.state.lastPathItem + "' elements"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.elements ? this.state.elements.map((item) => {
                      var theLink = "/fireadmin/" + this.state.completePath + Config.adminConfig.urlSeparatorFirestoreSubArray + item.uidOfFirebase;
                      return (<Link onClick={() => { this.showSubItems(theLink) }}><a className="btn">{item.uidOfFirebase}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}




          </div>
        </div>
        <SkyLight hideOnOverlayClicked ref="deleteDialog" title="">
          <span><h3 className="center-block">Delete data</h3></span>
          <div className="col-md-12">
            <Notification type="danger" >All data at this location, but not nested collections, will be deleted! To delete any collection's data go in each collection and detele the documents</Notification>
          </div>
          <div className="col-md-12">
            Data Location
          </div>
          <div className="col-md-12">
            <b>{this.state.pathToDelete + "/" + this.state.keyToDelete}</b>
          </div>

          <div className="col-sm-12" style={{ marginTop: 80 }}>
            <div className="col-sm-6">
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.cancelDelete} className="btn btn-info center-block">Cancel</a>
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.doDelete} className="btn btn-danger center-block">Delete</a>
            </div>

          </div>
        </SkyLight>

        <SkyLight hideOnOverlayClicked ref="addCollectionDialog" title="">
          <span><h3 className="center-block">Add first document in collection</h3></span>
          <div className="col-md-12">
            <Notification type="success" >Looks like there are no documents in this collection. Add your first document in this collection</Notification>
          </div>

          <div className="col-md-12">
            Data Location
          </div>
          <div className="col-md-12">
            <b>{this.state.showAddCollection}</b>
          </div>


          <div className="col-sm-12" style={{ marginTop: 80 }}>
            <div className="col-sm-6">
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.cancelAddFirstItem} className="btn btn-info center-block">Cancel</a>
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={() => { this.refs.addnewitemDialog.show() }} className="btn btn-success center-block">ADD</a>
            </div>

          </div>

        </SkyLight>

        {/*begin pp_add*/}
        <SkyLight dialogStyles={addnewitemDialog} hideOnOverlayClicked ref="addnewitemDialog">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">location_on</i>
              </div>
              <h4>Add New Athlete</h4>
              <div className="wizard-container">
                <div className="card wizard-card" data-color="red" id="wizard">
                  <form action="" method="" novalidate="novalidate">
                    <div className="wizard-navigation">
                      <ul className="nav nav-tabs">
                        <li className="active tabs-width"><a href="#about" data-toggle="tab" aria-expanded="false">About</a></li>
                        <li className="tabs-width"><a href="#contact" data-toggle="tab" aria-expanded="true">Contact</a></li>
                        <li className="tabs-width"><a href="#sport" data-toggle="tab">Sport</a></li>
                      </ul>
                      {/* <div className="moving-tab"> </div> */}
                    </div>

                    <div className="tab-content">
                      <div className="tab-pane active" id="about">
                        <div className="row">
                          <div className="col-sm-1"></div>
                          <div className="col-sm-6">
                            {/* Full Name */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right">
                                <label className="control-label label-color">Full Name :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="text" required="true" name="full_name" aria-required="true" value={this.state.full_name} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Full Name */}
                            {/* Date of Birth */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right">
                                <label className="control-label label-color" >Date Of Birth :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="text" required="true" name="date_of_birth" aria-required="true" value={this.state.date_of_birth} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Date of Birth */}
                            {/* Gender */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right">
                                <label className="control-label label-color" >Gender :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="text" required="true" name="gender" aria-required="true" value={this.state.gender} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Gender */}
                            {/* Aadhar number */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right padding-right-5">
                                <label className="control-label label-color" >Aadhar Number :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="text" name="aadhar_number" required="true" value={this.state.aadhar_number} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Aadhar number */}
                          </div>
                          {/* Photo */}
                          <div className="col-sm-4">
                            <Image updateAction={this.updateAction} theKey="photo" value={this.state.photo_url} />
                            {/* <div className="fileinput fileinput-new text-center" data-provides="fileinput">
                              <div className="fileinput-new thumbnail">
                                <img src="../../assets/img/image_placeholder.jpg" alt="..." />
                              </div>
                              <div className="fileinput-preview fileinput-exists thumbnail"></div>
                              <div>
                                <span className="btn btn-rose btn-round btn-file">
                                  <span className="fileinput-new">Select club image</span>
                                  <span className="fileinput-exists ">Change</span>
                                  <input type="file" name="..." />
                                </span>
                                <a href="#pablo" className="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i className="fa fa-times"></i> Remove</a>
                              </div>
                            </div> */}
                          </div>
                          {/* Photo */}
                          <div className="col-sm-1"></div>
                        </div>
                      </div>
                      <div className="tab-pane" id="contact">
                        <div className="row">
                          <div className="col-sm-6">
                            {/* E-mail */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right">
                                <label className="control-label label-color">E-Mail :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="email" required="true" name="email" aria-required="true" value={this.state.email} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* E-mail */}
                            {/* Mobile number */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right padding-right-5">
                                <label className="control-label label-color" >Mobile Number :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="text" name="mobile_number" required="true" value={this.state.mobile_number} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Mobile number */}
                            {/* address */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right">
                                <label className="control-label label-color" > Address :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <textarea required="true" name="address" value={this.state.address} onChange={this.handleInputChange} className="form-control" rows="4" cols="21" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* address */}
                          </div>
                          <div className="col-sm-6">
                            {/* Father Name */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right">
                                <label className="control-label label-color">Father's Name :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="text" required="true" name="father_name" aria-required="true" value={this.state.father_name} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Father Name */}
                            {/* Mother Name */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right padding-right-5">
                                <label className="control-label label-color" >Mother's Name :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="text" required="true" name="mother_name" aria-required="true" value={this.state.mother_name} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Mother number */}
                          </div>
                        </div>
                      </div>
                      <div className="tab-pane" id="sport">
                        <div className="row">
                          <div className="col-sm-6">
                            {/* Sport */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right">
                                <label className="control-label label-color">Sport :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <select name="sports">
                                      <option value="">Select Sports</option>
                                      <option value="swimming">Swimming</option>
                                      <option value="diving">Diving</option>
                                      <option value="water_polo">Water Polo</option>
                                    </select>
                                    {/* <input type="text" required="true" name="sports" aria-required="true" value={this.state.sports} onChange={this.handleInputChange} className="form-control" /> */}
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Sport */}
                            {/* Category */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right padding-right-5">
                                <label className="control-label label-color" >Category :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                  <select name="category">
                                      <option value="">Select Category</option>
                                      <option value="general">General</option>
                                      <option value="special">Special</option>
                                      <option value="Para">Para</option>
                                    </select>
                                    {/* <input type="text" required="true" name="category" aria-required="true" value={this.state.category} onChange={this.handleInputChange} className="form-control" /> */}
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Category */}
                            {/* Coach ID */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right padding-right-5">
                                <label className="control-label label-color" >Coach ID :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="text" required="true" name="coach_id" aria-required="true" value={this.state.coach_id} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Coach ID */}
                          </div>
                          <div className="col-sm-6">
                            {/* Documents */}
                            <div className="row margin-tb-15">
                              <div className="col-sm-4 text-align-right padding-right-5">
                                <label className="control-label label-color" >Documents :</label>
                              </div>
                              <div className="col-sm-8 margin-padding-0">
                                <div className="input-group display-block">
                                  <div className="form-group label-floating">
                                    <input type="text" required="true" name="name" aria-required="true" value={this.state.Fullname} onChange={this.handleInputChange} className="form-control" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* Documents */}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="wizard-footer">
                      <div className="pull-right">
                        <input type="button" className="btn btn-next btn-fill btn-danger btn-wd" name="next" value="Next" />
                        <input type="button" className="btn btn-finish btn-fill btn-danger btn-wd display-none" name="finish" value="Finish" />
                      </div>
                      <div className="pull-left">
                        <input type="button" className="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="Previous" />

                        <div className="footer-checkbox" >
                          <div className="col-sm-12">
                            <div className="checkbox">
                              <label>
                                <input type="checkbox" name="optionsCheckboxes" /><span className="checkbox-material"><span className="check"></span></span>
                              </label>
                              Subscribe to our newsletter
											  </div>
                          </div>
                        </div>
                      </div>
                      <div className="clearfix"></div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </SkyLight>

        {/* <form  onSubmit={this.addMessage} className="padding-10">
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Fullname">Full Name *:</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0">
                    <Input  required="required"
                         ClassName="form-control" 
                         type="text"   
                         theKey="Fullname"                       
                         placeholder= "Fullname"
                         value={this.state.Fullname} 
                         updateAction={this.formValueCapture}
                           /> 
                        <span style={{color: "red"}}>{this.state.errors["name"]}</span>
                    </div> 
          </div>
          
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Fathername">Father Name *:</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0">
                    <Input  
                      theKey="FatherName"  
                      type="text" 
                      updateAction={this.formValueCapture}
                      value={this.state.FatherName}
                      />
                    </div> 
          </div>
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Mothername">Mother Name *:</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0">
                    <Input 
                    type="text"
                    theKey="MotherName" 
                    updateAction={this.formValueCapture}
                    value={this.state.MotherName} />
                    </div> 
          </div>
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="DOB">DOB *:</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0">
                    <Input 
                    type="text"
                    theKey="DOB" 
                    updateAction={this.formValueCapture}
                    value={this.state.DOB} />
                    </div> 
          </div>
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Gender">Gender *:</label>
                    </div>
                    <div  className="col-sm-8 ">
                    <label className="radio-inline padding-top-10">                    
                      <input 
                        type="radio" 
                        theKey="Gender"
                        name="Gender"
                        updateAction={this.formValueCapture}                         
                        value="Male"  />Male</label>
                    <label className="radio-inline padding-top-10">
                      <input 
                        type="radio" 
                        name="Gender"
                        theKey="Gender" 
                        value="Female"                        
                        updateAction={this.formValueCapture}
                         />Female</label>
                    </div> 
          </div>
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Address">Address *:</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0">
                    <Input  
                      type="text" 
                      theKey="Address"
                      updateAction={this.formValueCapture}
                      value={this.state.Address}/>
                    </div> 
          </div>
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Mobileno">Mobile No *:</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0">
                    <Input 
                       type="text" 
                       theKey="Mobileno"
                       updateAction={this.formValueCapture}
                       value={this.state.Mobileno}/> 
                    </div> 
          </div>
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Email">E-Mail *:</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0">
                    <Input 
                      type="email" 
                      theKey="Email" 
                      updateAction={this.formValueCapture}
                      value={this.state.Email}
                      />
                      <span style={{color: "red"}}>{this.state.errors["email"]}</span>
                    </div> 
          </div>
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Aadharno">Aadhar Number *:</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0">
                    <Input 
                      type="number" 
                      theKey="Aadharno" 
                      updateAction={this.formValueCapture}
                      value={this.state.Aadharno}
                    />
                    </div> 
          </div>
           
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Passport">Passport Photo *:</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0 padding-top-10">
                    <File  
                        theKey="Passport" 
                        accept="image/*"
                        updateAction={this.formValueCapture}
                        value={this.state.Passport}
                     />
                    </div> 
          </div>
          <div className="row margin-tb-15">
                    <div className="col-sm-3 text-align-right"> 
                    <label className="control-label label-color" htmlFor="Otherdoc">Others Documents :</label>
                    </div>
                    <div className="col-sm-8 margin-padding-0 padding-top-10">
                    <File   
                      theKey="Otherdoc" 
                      accept="image/*"
                      updateAction={this.formValueCapture}
                      value={this.state.Otherdoc}/>
                    </div> 
          </div>
          <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                    <button className="btn btn-rose btn-width" value="Save"> <i className="material-icons">save</i> Save</button>
                    </div> 
                    
          </div>
            
          </form> */}

        {/*end pp_add*/}

        <SkyLight hideOnOverlayClicked ref="simpleDialog" title="">
          <span><h3 className="center-block">Add new key</h3></span>
          <br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Name of they key</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="NAME_OF_THE_NEW_KEY" value={"name"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div><br /><br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Value</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="VALUE_OF_THE_NEW_KEY" value={"value"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div>
          <div className="col-sm-12 ">
            <div className="col-sm-3 ">
            </div>
            <div className="col-sm-6 center-block">
              <a onClick={this.addKey} className="btn btn-rose btn-round center-block"><i className="fa fa-save"></i>   Add key</a>
            </div>
            <div className="col-sm-3 ">
            </div>
          </div>
        </SkyLight>
      </div>
    )
  }

}
export default Firestoreadmin;

