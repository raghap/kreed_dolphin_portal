import React, {Component,PropTypes} from 'react'
import {default as DT} from 'react-datetime'
import 'react-datetime/css/react-datetime.css'
import moment from 'moment'




class KreedTimePicker extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value:moment(props.value,"DD/MM/YYYY")
    };
    this.handleChange=this.handleChange.bind(this);
  }

  handleChange(event) {
   this.setState({value: event});

   //Convert to desired save format 
    var converted=moment(event).format('LT')
    // var converted=moment(event).format(this.props.dateFormats.saveAs)
    this.props.updateAction(this.props.theKey,converted,false,"date");
  }

  render() {
    return (
            <div className="form-group label-floating is-empty">
                <label className="control-label"></label>
                <DT inputProps={{placeholder:'HH:MM', readOnly:true}} 
                    onChange={this.handleChange} 
                    value={this.state.value} 
                    dateFormat={false} 
                    timeFormat={true} 
                    closeOnSelect={true}
                    locale={"en"}  />
            </div>
    )
  }
}
export default KreedTimePicker;
